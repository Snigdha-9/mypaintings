
  
Back-end: 

In paintings project, src/main/java com.painting.fav/FavApplicatoion.java run as java application(server runs at port 8080
setup data base with MySql workbench , create new schema ,create new table in that schema with column names id(primarykey,AI),name,url,favorite.

go to application.properties and change schema name spring.datasource.url=jdbc:mysql://127.0.0.1:3306/paintings 

front-end:
 
open at localhost:8080 

using the addnew button u can add a picture using the url(create operation)
on page load,i used GETAPI to call all the pictures (read operation)
once the picture is uploaded the picture can be edited(update operation)
with the delete option u can delete the picture (delete operation)

using the favourite button can also favourite the picture 
using the social share buttons can share the pictures.

