CREATE SCHEMA `paintings1` DEFAULT CHARACTER SET utf8mb4 ;

CREATE TABLE `paintings1`.`paint_table` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `url` VARCHAR(255) NOT NULL,
  `favorite` BOOLEAN NULL,
  PRIMARY KEY (`id`));

