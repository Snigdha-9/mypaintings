package com.painting.fav.painting;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PaintNotFound extends RuntimeException {

	public PaintNotFound(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	

}
