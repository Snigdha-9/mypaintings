package com.painting.fav.painting;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Value;

@Entity
@Table(name="paintings")
public class Painting {
		
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private Integer id;
		
		@Column(name="url") 
		private String url;
		
		@Size(min=2,max=30)
		@Column(name="name") 
		private String name;
		
		@Value("false")
		@Column(columnDefinition = "boolean default false",name="favorite") 
		private Boolean favorite=false;
		public Boolean getFavorite() {
			return favorite;
		}
		public void setFavorite(Boolean favorite) {
			this.favorite = favorite;
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		protected Painting() {
			
		}
		public Painting(Integer id, String url, String name,Boolean favorite) {
			super();
			this.id = id;
			this.url = url;
			this.name = name;
			this.favorite = favorite;
		}
		
		
}
