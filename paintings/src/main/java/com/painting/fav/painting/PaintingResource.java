package com.painting.fav.painting;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@CrossOrigin(origins = { "http://127.0.0.1:5500", "http://127.0.0.1:8080" })
@RestController
public class PaintingResource {
	
	@Autowired
	private PaintingService service;
	
	@GetMapping("/paintings")
	public List<Painting> getAllPaintings(){
		return service.findAll();
	}
	
	
	@GetMapping("/paintings/{id}")
	public Painting getPainting(@PathVariable int id){
		Painting paint=service.findOne(id);
		if(paint == null) {
			throw new PaintNotFound("id-"+id);
		}
		return service.findOne(id);
	}
	@DeleteMapping("/paintings/{id}")
	public void deleteUser(@PathVariable int id){
		Painting paint=service.deleteById(id);
		if(paint == null) {
			throw new PaintNotFound("id-"+id);
		}
		
	}
	
	@PostMapping("/paintings")
	public ResponseEntity addPainting(@Valid @RequestBody Painting paint) {
		Painting paintSaved=service.save(paint);
		
	URI location=	ServletUriComponentsBuilder
		.fromCurrentRequest()
		.path("/{id}")
		.buildAndExpand(paintSaved.getId())
		.toUri();
		
		return ResponseEntity.created(location).build();
		
	}
}
