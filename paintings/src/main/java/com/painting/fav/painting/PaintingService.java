package com.painting.fav.painting;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.springframework.stereotype.Component;



@Component
public class PaintingService {
	private static List<Painting> paints=new ArrayList<>();
	private int noOfPaints=5;
	static {
		paints.add(new Painting(1,"http://mymodernmet.com/wp/wp-content/uploads/2018/05/painting-ideas-3-1.jpg","test",false));
		paints.add(new Painting(2,"http://mymodernmet.com/wp/wp-content/uploads/2018/05/painting-ideas-3-1.jpg","test",false));
		paints.add(new Painting(3,"http://mymodernmet.com/wp/wp-content/uploads/2018/05/painting-ideas-3-1.jpg","test",false));
		paints.add(new Painting(4,"http://mymodernmet.com/wp/wp-content/uploads/2018/05/painting-ideas-3-1.jpg","test",false));
		paints.add(new Painting(5,"http://mymodernmet.com/wp/wp-content/uploads/2018/05/painting-ideas-3-1.jpg","test",false));
	}

	public List<Painting> findAll(){
		return paints;
	}
	public Painting save(Painting paint){
	    if(paint.getId() == null) {
			paint.setId(++noOfPaints);
		}
		paints.add(paint);
		return paint;
	}
	public Painting findOne(int id) {
		for(Painting paint:paints) {
			if(paint.getId() == id) {
				return paint;
			}
		}
		return null;
	}
	public Painting deleteById(int id) {
		Iterator<Painting> iterator=paints.iterator();
		while(iterator.hasNext()) {
			Painting paint=iterator.next();
			if(paint.getId() == id) {
				iterator.remove();
				return paint;
			}
		}
		return null;
	}
	

	
}
