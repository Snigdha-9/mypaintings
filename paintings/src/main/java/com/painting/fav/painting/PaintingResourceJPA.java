package com.painting.fav.painting;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@CrossOrigin(origins = { "http://127.0.0.1:5500", "http://127.0.0.1:8080" })
@RestController
public class PaintingResourceJPA {
	

	@Autowired
	private PaintingRepository paintingRepository;
	
	@GetMapping("/jpa/paintings")
	public List<Painting> getAllPaintings(){
		return paintingRepository.findAll();
	}
	
	
	@GetMapping("/jpa/paintings/{id}")
	public Painting getPainting(@PathVariable int id){
		Optional<Painting> paint=paintingRepository.findById(id);
		
		if(!paint.isPresent()) {
			throw new PaintNotFound("id-"+id);
		}
		return paint.get();
	}
	@DeleteMapping("/jpa/paintings/{id}")
	public void deletePainting(@PathVariable int id){
		paintingRepository.deleteById(id);
		
	}
	
	@PutMapping("/jpa/paintings/{id}")
	public ResponseEntity<Object> updatePainting(@RequestBody Painting paint ,@PathVariable int id){
		Optional<Painting> paintOptional=paintingRepository.findById(id);
		if(!paintOptional.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		paint.setId(id);
		paintingRepository.save(paint);
		return ResponseEntity.noContent().build();
		
	}
	@PostMapping("/jpa/paintings")
	public ResponseEntity<Object> addPainting(@Valid @RequestBody Painting paint) {
		Painting paintSaved=paintingRepository.save(paint);
		
	URI location=	ServletUriComponentsBuilder
		.fromCurrentRequest()
		.path("/{id}")
		.buildAndExpand(paintSaved.getId())
		.toUri();
		
		return ResponseEntity.created(location).build();
		
	}
}
