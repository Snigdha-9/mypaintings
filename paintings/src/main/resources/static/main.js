const modal=document.getElementById("modal");
const modalClose=document.getElementById("modal-close");
const modalForm=document.getElementById("modalForm");
const modalFile=document.getElementById("modal-file");

const home=document.getElementById("home");
const favorite=document.getElementById("favorite");
var fileUrl;
const urlRoot="http://localhost:8080/jpa/paintings/";
var imagesList;
getAllPaintings();
home.classList.add('active');


home.addEventListener('click',function(){
    getAllPaintings();
    home.classList.add('active');
    favorite.classList.remove('active');
})
favorite.addEventListener('click',function(){
    getAllPaintings(true)
    home.classList.remove('active');
    favorite.classList.add('active')
})


function getAllPaintings(param){
    
    fetch(urlRoot, {
        method: 'GET',
    }).then(function(res){
      return  res.json();
    })
    .then(function (data)  {
        if(param){
            data= data.filter(function(value){
                return value.favorite === true;
            })
        }
        appendPaintings(data)
    }).catch(function(){
        appendPaintings([]);
    });
}

function appendPaintings(imageList){
    const galleryContainer=document.getElementById("galleryContainer");
    console.log(imageList)
    galleryContainer.innerHTML="";
    imagesList=imageList;
    if(imageList.length>0){

        // const addDiv=document.createElement("figure");
        // addDiv.id="add-new"
        // addDiv.classList.add("add-new");
        // const addIcon=document.createElement("i");
        // addIcon.classList.add("fa");
        // addIcon.classList.add("fa-plus");
        // addDiv.appendChild(addIcon);
        // addDiv.addEventListener('click',function(e){
        //     addPainting();
        // });
        // galleryContainer.appendChild(addDiv);

        for (let index = 0; index < imageList.length; index++) {
            const imageObj = imageList[index];
            const img=document.createElement("img");
            img.src=imageObj.url;
            img.alt=imageObj.name;
            const div=document.createElement("figure");
            div.id=imageObj.id;

            const innerDiv=document.createElement("div");
            innerDiv.classList.add("img-desc");

            const title=document.createElement("span");
            title.classList.add("img-title");
            title.innerHTML=imageObj.name;
            innerDiv.appendChild(title);

            const favIcon=document.createElement("i");
            favIcon.classList.add("fa");
            favIcon.classList.add("fa-heart");
            if(imageObj.favorite){
             favIcon.classList.add("favorite");
            }
            favIcon.addEventListener('click',function(e){
                favPainting(imageObj)
            });

            innerDiv.appendChild(favIcon);

            const overlay=document.createElement("div");
            overlay.classList.add("img-overlay");
            
        const actionIcons=document.createElement("div");
        actionIcons.classList.add("img-action");
        const editIcon=document.createElement("i");
           editIcon.classList.add("fa");
           editIcon.classList.add("fa-edit");
           editIcon.classList.add("edit-icon");
           
           editIcon.addEventListener('click',function(){
            editPainting(imageObj);
           });
           actionIcons.appendChild(editIcon);

           const deleteIcon=document.createElement("i");
           deleteIcon.classList.add("fa");
           deleteIcon.classList.add("fa-trash");
           deleteIcon.classList.add("delete-icon");
           deleteIcon.addEventListener('click',function(){
            deletePainting(imageObj);
           });
           actionIcons.appendChild(deleteIcon);

           const viewIcon=document.createElement("i");
           viewIcon.classList.add("fa");
           viewIcon.classList.add("fa-eye");
           viewIcon.classList.add("view-icon");
           viewIcon.addEventListener('click',function(){
            viewPainting(imageObj);
           });
           
         
         
           const socialIconDiv=document.createElement("div");
           socialIconDiv.classList.add("social-icons");
     
           const linkedInIcon=document.createElement("i");
           linkedInIcon.classList.add("fa");
           linkedInIcon.classList.add("fa-linkedin");
           linkedInIcon.addEventListener('click',function(){
               window.open("https://www.linkedin.com/sharing/share-offsite/?url="+imageObj.url)
           })
           socialIconDiv.appendChild(linkedInIcon);

           const fbIcon=document.createElement("i");
           fbIcon.classList.add("fa");
           fbIcon.classList.add("fa-facebook");
           fbIcon.addEventListener('click',function(){
               window.open("https://www.facebook.com/sharer/sharer.php?u="+imageObj.url)
           })
           socialIconDiv.appendChild(fbIcon);

         
     
           const tweetIcon=document.createElement("i");
           tweetIcon.classList.add("fa");
           tweetIcon.classList.add("fa-twitter");
           tweetIcon.addEventListener('click',function(){
             window.open("https://twitter.com/intent/tweet?text="+imageObj.name+"&url="+imageObj.url)
         })
           socialIconDiv.appendChild(tweetIcon);
          
     
       
     
            div.appendChild(img);
            div.appendChild(innerDiv);
            div.appendChild(actionIcons);
            div.appendChild(viewIcon);
            div.appendChild(socialIconDiv);
            div.appendChild(overlay);
         
        //     div.appendChild(innerDiv);
            
            galleryContainer.appendChild(div);
        }
        galleryContainer.classList.add('gallery-container');
        galleryContainer.classList.remove('no-image');
    }else{
        const p=document.createElement("p");
        p.innerHTML="No Images Found, Add some";
        galleryContainer.appendChild(p);
        galleryContainer.classList.add('no-image');
        galleryContainer.classList.remove('gallery-container');
    }
}

function favPainting(imageObj){
    console.log(imageObj)
    const formValues={
        name:imageObj.name,
        url:imageObj.url,
        favorite:!imageObj.favorite
    }
    fetch(urlRoot + imageObj.id, {
        method: 'PUT',
        body: JSON.stringify(formValues),
        headers:{
            "Content-Type": "application/json"
        }
    }).then(function(res){
        getAllPaintings();
    })
}
function addPainting(){
    modal.style.display="block";
    const modalContent= document.getElementById('modalContent');
    modalContent.style.width="20vw";
    modalContent.style.height="30vh";
    modalContent.style.margin="15% auto";
}
function editPainting(imageObj){
    modal.style.display="block";
    const formEl = document.forms.modalForm;
    formEl.elements["name"].value=imageObj.name;
    formEl.elements["url"].value=imageObj.url;
   const modalSubmit= document.getElementById('modal-submit');
   modalSubmit.setAttribute('edit',true);
   modalSubmit.setAttribute('index',imageObj.id);
}
function deletePainting(imageObj){
    const id=imageObj.id;
    fetch(urlRoot + id, {
        method: 'DELETE',
    }).then(function(res){
        getAllPaintings();
        return res.json();
    })
}

function viewPainting(imageObj){
    const modalView= document.getElementById('modalView');
    const modalTitle= document.querySelector('.modal-title span:nth-of-type(1)');
    const img=document.createElement("img");
    img.src=imageObj.url;
    img.alt=imageObj.name;
    modalTitle.innerHTML="View - "+imageObj.name;
    modalView.append(img);
    modalView.style.display="flex";
    modalView.classList.add("modal-view");
    modal.style.display="block";
    modalForm.style.display="none";
    const modalContent= document.getElementById('modalContent');
    modalContent.style.width="30vw";
    modalContent.style.height="60vh";
    modalContent.style.margin="5% auto";
   
      
}

modalClose.addEventListener('click',onModalClose)
function onModalClose(){
    const modalView= document.getElementById('modalView');
    const modalTitle= document.querySelector('.modal-title span:nth-of-type(1)');
    modalTitle.innerHTML="Add New Paint";
    modalView.innerHTML="";
    modalView.style.display="none";
    modalView.classList.remove("modal-view");
    modalForm.style.display="block";

    modal.style.display="none";
    const formEl = document.forms.modalForm;
    formEl.elements["name"].value="";
    formEl.elements["url"].value="";
    const modalSubmit= document.getElementById('modal-submit');
    modalSubmit.removeAttribute('edit');
   modalSubmit.removeAttribute('index');
}

modalForm.addEventListener('submit',function(event){
    event.preventDefault();
    const formEl = document.forms.modalForm;
    const formData = new FormData(formEl);
    const modalSubmit= document.getElementById('modal-submit');
    const formValues={
        name:formData.get('name'),
        url:formData.get('url'),
        image:fileUrl
    }
    console.log(formValues,'formValues')
    if(modalSubmit.getAttribute('edit')){
        fetch(urlRoot + modalSubmit.getAttribute('index'), {
            method: 'PUT',
            body: JSON.stringify(formValues),
            headers:{
                "Content-Type": "application/json"
            }
        }).then(function(res){
            getAllPaintings();
            onModalClose();
        })
    }else{
        fetch(urlRoot, {
            method: 'POST',
            body: JSON.stringify(formValues),
            headers:{
                "Content-Type": "application/json"
            }
        }).then(function(res){
            getAllPaintings();
            onModalClose();
        })
    }
    
    

   
   
})
function encodeImageFileAsURL(element) {
    var file = element.files[0];
    var reader = new FileReader();
    reader.onloadend = function() {
      fileUrl=reader.result;
    }
    reader.readAsDataURL(file);
  }
